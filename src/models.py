class Carro:
    
    modelo: str
    placa: str

    def __init__(self, modelo: str, placa: str) -> None:
        self.modelo = modelo
        self.placa = placa

    def getModelo(self):
        return self.modelo

    def getPlaca(self):
        return self.placa